const logLevel = ["DEBUG", "INFO", "ERROR"];
const DEBUG = "DEBUG", INFO = "INFO", ERROR = "ERROR";

export class Logger {
	/** @type {HTMLDivElement} */
	#element;
	
	constructor(logsElement) {
		this.#element = logsElement;
	}
	
	log(level, msg) {
		const elem = document.createElement("code");
		elem.className = (typeof level === "string") ? level : logLevel.at(level);
		elem.innerText = `[${level}] ${msg}`;
		this.#element.appendChild(elem);
	}
	
	clear() {
		while (this.#element.children.length) {
			this.#element.removeChild(this.#element.lastChild);
		}
	}
	
	debug(msg) { this.log(DEBUG, msg); }
	info(msg)  { this.log(INFO,  msg); }
	error(msg) { this.log(ERROR, msg); }
}
