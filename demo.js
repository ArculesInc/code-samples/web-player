import { Logger } from "./log.js";
// Setup logger
const logger = new Logger(document.getElementById('logs'));

// Set up VideoChunk
let VideoChunk;
let socket;
try {
  const root = await protobuf.load('./VideoChunk.proto');
  VideoChunk = root.lookupType("VideoChunk");
}catch (e) {
  logger.error('Failed to load protobuf');
}

window.onload = () => {
  document
    .getElementById("startButton")
    .addEventListener("click", startPlayback);
  
  document
    .getElementById("startLiveButton")
    .addEventListener("click", startLive);
  
  // Set default start time to 2 minutes prior to now
  const now = new Date()
  const defaultTime = new Date(now.setMinutes(now.getMinutes() - 2)).toISOString();
  const startTimeInput = document.getElementById("startTime");
  
  startTimeInput.value = defaultTime.slice(0, defaultTime.length - 8);

  const cookies = document.cookie.split("; ");
  
  // Getting values from cookies for simplicity
  for (let cookie of cookies) {
    if (cookie.startsWith("jwt="))
      document.getElementById("jwt").value = cookie.split("=")[1];
    if (cookie.startsWith("chguid="))
      document.getElementById("channelGUID").value = cookie.split("=")[1];
    if (cookie.startsWith("addr="))
      document.getElementById("SERVER_HOST").value = cookie.split("=")[1];
    if (cookie.startsWith("org="))
      document.getElementById("org").value = cookie.split("=")[1];
  }
};

function startPlayer(url, message) {
  document.getElementById("errorBox").classList.replace("shown", "hidden");
  const video = document.getElementById("remoteVideo");
  video.classList.replace("hidden", "shown");
  
  URL.revokeObjectURL(video.src);
  
  const mediaSource = new MediaSource();
  video.src = URL.createObjectURL(mediaSource);
  
  mediaSource.addEventListener('sourceopen', sourceOpen, { once: true });
  
  function sourceOpen() {
    logger.info('Set Up Media Source');
    const AVC1 = 'video/mp4;codecs="avc1.640028"'; // H.264 Main Profile level 3.0
    
    if (!window.MediaSource && !MediaSource.isTypeSupported(AVC1)) {
      logger.error('MediaSource is not supported by the browser!');
    }
    const sourceBuffer = mediaSource.addSourceBuffer(AVC1);
    sourceBuffer.mode = 'sequence';
    logger.info('Added Source Buffer');
    
    socket = new WebSocket(url);
    socket.binaryType = 'arraybuffer';
    

    socket.onopen = function() {
      logger.info('WebSocket Connection Established');
      // Send request to BE for playback
      if (message) {
        socket.send(message);
      }
    };
    
    let nextTimestamp;
    let prevTimestamp;
    socket.onmessage = function(event) {
      if (mediaSource.readyState === 'open') {
        const chunk = VideoChunk.decode(new Uint8Array(event.data));
        
        if (chunk.error) {
          const errMsg = `Error from gateway: ${chunk.error.errorCode} - ${chunk.error.errorText}`;
          logger.error(errMsg);
        }
        if (chunk.isLastChunk) {
          logger.info('Received last chunk');
          
          const direction = parseInt(document.getElementById('directionToPlay').value);
          // Request more if user selected "Forward"
          if (direction === 1) {
            if (nextTimestamp) {
              logger.info('Requesting next timestamp');
              const body = prepareCursorRequest(nextTimestamp);
              socket.send(body);
              nextTimestamp = null;
            } else {
              logger.info('Reached the end of the recording');
            }
          }
        } else {
          nextTimestamp = chunk.nextTimestamp;
          prevTimestamp = chunk.previousTimestamp;
          sourceBuffer.appendBuffer(chunk.data);
        }
        
      }
    };
    
    socket.onclose = function() {
      logger.info('WebSocket Connection Closed');
      if (mediaSource.readyState === 'open') {
        mediaSource.endOfStream();
      }
    };
  }
}

function startLive() {
  setCookies();
  
  const server = document.getElementById('SERVER_HOST').value;
  const channel = document.getElementById('channelGUID').value;
  const token = document.getElementById('jwt').value;
  const org = document.getElementById('org').value;
  const url = `wss://${server}/v1/channels/${channel}/live?token=${token}&org_id=${org}&format=proto`;
  logger.info(`URL: ${url}`);
  startPlayer(url);
}

function startPlayback() {
  setCookies();
  if (socket) {
    socket.close();
  }
  
  const server = document.getElementById('SERVER_HOST').value;
  const channel = document.getElementById('channelGUID').value;
  const token = document.getElementById('jwt').value;
  const org = document.getElementById('org').value;
  const url = `wss://${server}/v1/channels/${channel}/playback?token=${token}&org_id=${org}&format=proto`;
  logger.info(`URL: ${url}`);

  startPlayer(url, prepareCursorRequest());
}

function prepareCursorRequest(time) {
  const timestamp = document.getElementById('startTime').value;
  const gop = document.getElementById('gopCount').value;
  const direction = document.getElementById('directionToPlay').value;
  const body = {
    timestamp: time || Date.parse(new Date(Date.parse(timestamp)).toLocaleString() + " GMT-0000"),
    gop_count: parseInt(gop),
    direction: parseInt(direction)
  }
  return JSON.stringify(body);
}

function setCookies() {
  const _8hrs = 8 * 3600;
  logger.info('Remember Values In Cookies');
  
  document.cookie = `jwt=${
    document.getElementById("jwt").value
  }; max-age=${_8hrs}; same-site=strict;`;
  document.cookie = `chguid=${
    document.getElementById("channelGUID").value
  }; max-age=${_8hrs}; same-site=strict;`;
  document.cookie = `addr=${
    document.getElementById("SERVER_HOST").value
  }; max-age=${_8hrs}; same-site=strict;`;
  document.cookie = `org=${
    document.getElementById("org").value
  }; max-age=${_8hrs}; same-site=strict;`;
  
}
