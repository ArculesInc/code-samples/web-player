# Web Player Demo

The Web Player Demo showcases the seamless integration of Arcules' Open API, allowing developers to effortlessly stream and review live and playback feeds from any video channel.

## Requirements

- Node
- Npm or Yarn

## Installation

```bash
git clone https://gitlab.com/ArculesInc/code-samples/web-player
cd web-player
yarn install
```

## Usage

```bash
yarn start
```

Open browser and navigate to http://0.0.0.0:3001

## License

MIT

## Disclaimer

No Warranty, Use at Your Own Risk
This project is provided “as-is” and without any warranty. Use it at your own risk. The author and contributors are not responsible for any damages or liabilities that may arise from using this project. This is merely an example implementation and does not imply long-term support or maintenance.