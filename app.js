
const hostname = '0.0.0.0';  // listen on all interfaces
const port = 3001;

var express = require('express');
var path = require('path');
var app = express();
app.use(express.static(__dirname));

app.get('/', function (_, res) {
  res.sendFile(path.join(__dirname, 'demo.html'));
});

app.listen(port, () => console.log(`Server running at http://${hostname}:${port}`));
